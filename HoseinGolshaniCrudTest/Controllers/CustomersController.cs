﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services.Common;
using Services.Customers;
using Services.Customers.Dtos;

namespace HoseinGolshaniCrudTest.Controllers
{
    /// <summary>
    /// Customer CRUD
    /// </summary>
    [Route("[controller]")]
    public class CustomersController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Displays list of all customers
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> Index(HasCancellationToken input)
        {
            var outputs = await _customerService.GetCustomers(input);
            return View(outputs);
        }

        /// <summary>
        /// Display specific customer details 
        /// </summary>
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Details(GetCustomerInput input)
        {
            var customer = await _customerService.GetCustomer(input);

            if (customer == null)
                return NotFound();

            return View(customer);
        }

        /// <summary>
        /// Creates a new customer 
        /// </summary>
        [HttpGet("[action]")]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Creates a new customer 
        /// </summary>
        [HttpPost("[action]")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm] PostCustomerInput input)
        {
            if (!TryValidateModel(input))
                return View(input);

            try
            {
                await _customerService.PostCustomer(input);
            }
            catch (UniqueIndexOrConstraintException ex)
            {
                ModelState.AddModelError(ex.FieldName, ex.Message);
                return View(input);
            }

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Displays customer delete page 
        /// </summary>
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Delete(GetCustomerInput input)
        {
            var customer = await _customerService.GetCustomer(input);

            return View(customer);
        }

        /// <summary>
        /// Creates a new customer 
        /// </summary>
        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(GetCustomerInput input)
        {
            await _customerService.DeleteCustomer(input);
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Updates a specific customer
        /// </summary>
        [HttpPost("[action]/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PutCustomerInput input)
        {
            if (!TryValidateModel(input))
                return View("Create", input);

            try
            {
                await _customerService.PutCustomer(input);
            }
            catch (UniqueIndexOrConstraintException ex)
            {
                ModelState.AddModelError(ex.FieldName, ex.Message);
                return View("Create", input);
            }

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Creates a new customer 
        /// </summary>
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Edit(GetCustomerInput input)
        {
            var customer = await _customerService.GetCustomer(input);
            return View("Create", customer);
        }
    }
}