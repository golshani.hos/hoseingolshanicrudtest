﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using HoseinGolshaniCrudTest.Models;

namespace HoseinGolshaniCrudTest.Controllers
{
    public class HomeController : Controller
    {
        [ResponseCache(Duration = 60 * 60 * 24)] // 24 hour
        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 60 * 60 * 24)] // 24 hour
        [Route("/error/404")]
        public IActionResult Error404()
        {
            return View("404");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}