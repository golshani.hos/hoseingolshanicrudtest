using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace HoseinGolshaniCrudTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseUrls("http://127.0.0.1:9000")
                        .UseStartup<Startup>();
                });
    }
}