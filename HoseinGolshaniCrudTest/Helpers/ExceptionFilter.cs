﻿using Microsoft.AspNetCore.Mvc.Filters;
using Services.Common;

namespace HoseinGolshaniCrudTest.Helpers
{
    /// <summary>
    /// When NotFoundException occurs, set status code to 404
    /// </summary>
    public class NotFoundExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (!(context.Exception is NotFoundException)) return;

            context.HttpContext.Response.StatusCode = 404;
            context.ExceptionHandled = true;
        }
    }
}