﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Services.Common;

namespace HoseinGolshaniCrudTest.Helpers
{
    public class TrimStringModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
                throw new ArgumentNullException(nameof(bindingContext));

            if (bindingContext.ModelType != typeof(string))
                return Task.CompletedTask;

            var hasNoTrimAttribute = (bindingContext.ModelMetadata as DefaultModelMetadata)
                ?.Attributes.Attributes
                .Any(r => r is NoTrimAttribute);

            if (hasNoTrimAttribute == true)
                return Task.CompletedTask;

            var modelName = GetModelName(bindingContext);

            var valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
            if (valueProviderResult == ValueProviderResult.None)
                return Task.CompletedTask;

            bindingContext.ModelState.SetModelValue(modelName, valueProviderResult);

            var stringToParse = valueProviderResult.FirstValue;

            if (string.IsNullOrEmpty(stringToParse))
                return Task.CompletedTask;

            bindingContext.Result = ModelBindingResult.Success(stringToParse.Trim());

            return Task.CompletedTask;
        }

        private string GetModelName(ModelBindingContext bindingContext)
        {
            // The "Name" property of the ModelBinder attribute can be used to specify the
            // route parameter name when the action parameter name is different from the route parameter name.
            // For instance, when the route is /api/{birthDate} and the action parameter name is "date".
            // We can add this attribute with a Name property [DateTimeModelBinder(Name ="birthDate")]
            // Now bindingContext.BinderModelName will be "birthDate" and bindingContext.ModelName will be "date"
            return !string.IsNullOrEmpty(bindingContext.BinderModelName)
                ? bindingContext.BinderModelName
                : bindingContext.ModelName;
        }
    }

    public class TrimStringModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            return context.Metadata.ModelType == typeof(string)
                ? new TrimStringModelBinder()
                : null;
        }
    }
}