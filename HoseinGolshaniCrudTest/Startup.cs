using AutoMapper;
using Data;
using HoseinGolshaniCrudTest.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services.Common;
using Services.Customers;

namespace HoseinGolshaniCrudTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<ApplicationDbContext>(setup =>
            {
                setup.UseSqlServer(Configuration.GetConnectionString("Default"));
            });

            // Config AutoMapper using attributes in these assemblies
            services.AddAutoMapper(typeof(Startup), typeof(Default));

            services.AddScoped<ICustomerService, CustomerService>();

            services.AddControllersWithViews(setup =>
            {
                setup.Filters.Add<NotFoundExceptionFilter>();
                setup.ModelBinderProviders.Insert(0,new TrimStringModelBinderProvider());
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) 
                app.UseDeveloperExceptionPage();

            app.UseStatusCodePagesWithRedirects("/error/{0}");
            app.UseStaticFiles();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}