﻿using System.ComponentModel.DataAnnotations;

namespace Services.Customers.Dtos
{
    public class PutCustomerInput : PostCustomerInput
    {
        [Required]
        public int Id { get; set; }
    }
}