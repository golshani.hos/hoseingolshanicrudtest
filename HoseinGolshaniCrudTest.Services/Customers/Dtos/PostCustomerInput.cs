﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Data.Models;
using Services.Common;

namespace Services.Customers.Dtos
{
    [AutoMap(typeof(Customer), ReverseMap = true)]
    public class PostCustomerInput : HasCancellationToken
    {
        [Required]
        [MaxLength(Customer.FirstAndLastNameMaxLength)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(Customer.FirstAndLastNameMaxLength)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Birth Day")]
        [DateTimeRange(Maximum = DateTimeRangeType.Now)]
        public DateTime DateOfBirth { get; set; }

        [PhoneNumber]
        [Display(Name = "Phone Number")]
        [Required]
        public string PhoneNumber { get; set; }

        [EmailAddress]
        [MaxLength(Customer.EmailMaxLength)]
        public string Email { get; set; }

        [MaxLength(Customer.BankAccountNumberMaxLength)]
        [Display(Name = "Bank Account Number")]
        public string BankAccountNumber { get; set; }
    }
}