﻿using System.ComponentModel.DataAnnotations;
using Services.Common;

namespace Services.Customers.Dtos
{
    public class DeleteCustomerInput : HasCancellationToken
    {
        [Required]
        public int Id { get; set; }
    }
}