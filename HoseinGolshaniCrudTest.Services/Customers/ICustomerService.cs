﻿using System.Threading.Tasks;
using Services.Common;
using Services.Customers.Dtos;

namespace Services.Customers
{
    /// <summary>
    /// Customer CRUD
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// Creates a new customer
        /// </summary>
        /// <exception cref="UniqueIndexOrConstraintException">When duplicate customer tries to add</exception>
        Task PostCustomer(PostCustomerInput input);
      
        /// <summary>
        /// Updates customer 
        /// </summary>
        /// <exception cref="NotFoundException">If customer id not exists or already deleted</exception>
        Task PutCustomer(PutCustomerInput input);
        
        /// <summary>
        /// Tag customer as deleted 
        /// </summary>
        /// <exception cref="NotFoundException">If customer id not exists or already deleted</exception>
        Task DeleteCustomer(DeleteCustomerInput input);
        
        /// <summary>
        /// Get customer details
        /// </summary>
        /// <exception cref="NotFoundException">If customer id not exists or already deleted</exception>
        Task<GetCustomerOutput> GetCustomer(GetCustomerInput input);
        
        /// <summary>
        /// Get all customers
        /// </summary>
        Task<GetCustomerOutput[]> GetCustomers(HasCancellationToken input);
        
    }
}