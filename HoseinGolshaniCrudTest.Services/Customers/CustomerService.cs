﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Data;
using Data.Helpers;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Services.Common;
using Services.Customers.Dtos;

namespace Services.Customers
{
    /// <summary>
    /// Customer CRUD
    /// </summary>
    public class CustomerService : ICustomerService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper _mapper;

        public CustomerService(ApplicationDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates a new customer
        /// </summary>
        /// <exception cref="UniqueIndexOrConstraintException">When duplicate customer tries to add</exception>
        public async Task PostCustomer(PostCustomerInput input)
        {
            _dbContext.Customers.Add(_mapper.Map<Customer>(input));

            try
            {
                await _dbContext.SaveChangesAsyncEnsureAffectedRows(input.CancellationToken);
            }
            catch (Exception ex)
            {
                _throwIfUniqueIndexViolated(ex);
                throw;
            }
        }

        private void _throwIfUniqueIndexViolated(Exception ex)
        {
            switch (ex.GetViolatedConstraintName())
            {
                case null:
                    break;
                
                case UniqueConstraints.CustomersEmail:
                    throw new UniqueIndexOrConstraintException(nameof(PostCustomerInput.Email), "This email is already registered!");
                
                case UniqueConstraints.CustomersFirstNameLastNameDateOfBirth:
                    throw new UniqueIndexOrConstraintException(nameof(PostCustomerInput.FirstName), "The combination of 'First Name, Last Name, and Birth Day' is already registered!");
            }
        }

        /// <summary>
        /// Updates customer 
        /// </summary>
        /// <exception cref="NotFoundException">If customer id not exists or already deleted</exception>
        /// <exception cref="UniqueIndexOrConstraintException">When duplicate customer tries to add</exception>
        public async Task PutCustomer(PutCustomerInput input)
        {
            var customerToUpdate = await _dbContext.Customers
                                       .WhereExists(r => r.Id == input.Id)
                                       .FirstOrDefaultAsync(input.CancellationToken)
                                   ?? throw new NotFoundException();

            _mapper.Map(input, customerToUpdate);
            customerToUpdate.LastModificationTime = DateTime.Now;

            try
            {
                await _dbContext.SaveChangesAsyncEnsureAffectedRows(input.CancellationToken);
            }
            catch (Exception ex)
            {
                _throwIfUniqueIndexViolated(ex);
                throw;
            }
        }

        /// <summary>
        /// Tag customer as deleted 
        /// </summary>
        /// <exception cref="NotFoundException">If customer id not exists or already deleted</exception>
        public async Task DeleteCustomer(DeleteCustomerInput input)
        {
            var customer = await _dbContext.Customers
                               .WhereExists(r => r.Id == input.Id)
                               .FirstOrDefaultAsync(input.CancellationToken)
                           ?? throw new NotFoundException();

            customer.DeletionTime = DateTime.Now;
            customer.IsDeleted = true;

            await _dbContext.SaveChangesAsyncEnsureAffectedRows(input.CancellationToken);
        }

        /// <summary>
        /// Get customer details
        /// </summary>
        /// <exception cref="NotFoundException">If customer id not exists or already deleted</exception>
        public async Task<GetCustomerOutput> GetCustomer(GetCustomerInput input)
        {
            var output = await _dbContext.Customers
                             .AsNoTracking()
                             .WhereExists(r => r.Id == input.Id)
                             .ProjectTo<GetCustomerOutput>(_mapper.ConfigurationProvider)
                             .FirstOrDefaultAsync(input.CancellationToken)
                         ?? throw new NotFoundException();

            // add '+' to phone number for UI purpose
            if (output.PhoneNumber != null)
                output.PhoneNumber = "+" + output.PhoneNumber;

            return output;
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        public Task<GetCustomerOutput[]> GetCustomers(HasCancellationToken input)
        {
            return _dbContext.Customers
                .AsNoTracking()
                .WhereExists()
                .OrderByDescending(r => r.Id)
                .ProjectTo<GetCustomerOutput>(_mapper.ConfigurationProvider)
                .ToArrayAsync(input.CancellationToken);
        }
    }
}