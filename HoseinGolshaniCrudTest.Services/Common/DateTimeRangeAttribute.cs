﻿using System;
using System.ComponentModel.DataAnnotations;
using PhoneNumbers;

namespace Services.Common
{
    /// <summary>
    /// Make sure DateTime range is correct
    /// </summary>
    public class DateTimeRangeAttribute : ValidationAttribute
    {
        /// <summary>
        /// When is the minimum datetime
        /// </summary>
        public DateTimeRangeType Minimum { get; set; } = DateTimeRangeType.NoLimit;

        // When is the maximum datetime
        public DateTimeRangeType Maximum { get; set; } = DateTimeRangeType.NoLimit;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;

            if (!(value is DateTime))
                throw new Exception(validationContext.MemberName + " is not a DateTime!");

            if (Minimum != DateTimeRangeType.NoLimit && !_isOk(Minimum, (DateTime) value, true))
                return new ValidationResult(ErrorMessage ?? $"{validationContext.DisplayName} is not in correct range");
            
            if (Maximum != DateTimeRangeType.NoLimit && !_isOk(Maximum, (DateTime) value, false))
                return new ValidationResult(ErrorMessage ?? $"{validationContext.DisplayName} is not in correct range");
            
            return ValidationResult.Success;
        }

        private bool _isOk(DateTimeRangeType rangeType, DateTime dateTime, bool trueForCheckMinimum)
        {
            return rangeType switch
            {
                DateTimeRangeType.Now => (trueForCheckMinimum ? dateTime >= DateTime.Now : dateTime <= DateTime.Now),
                _ => throw new ArgumentOutOfRangeException(nameof(rangeType), rangeType, null)
            };
        }
    }
}