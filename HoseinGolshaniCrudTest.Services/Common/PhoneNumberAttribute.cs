﻿using System;
using System.ComponentModel.DataAnnotations;
using PhoneNumbers;

namespace Services.Common
{
    /// <summary>
    /// Make sure phone number is correct and contains '+', country code and number
    /// </summary>
    public class PhoneNumberAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;

            PhoneNumber phoneNumber;
            try
            {
                phoneNumber = PhoneNumberUtil.GetInstance().Parse(value.ToString(), null);
            }
            catch (Exception)
            {
                return new ValidationResult(
                    "Phone Number is not well-formatted. It must contains country code and starts with '+'");
            }

            if (!phoneNumber.HasCountryCode)
                return new ValidationResult("Phone number must contains country code");

            return ValidationResult.Success;
        }
    }
}