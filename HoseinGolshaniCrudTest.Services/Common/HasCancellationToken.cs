﻿using System.Threading;
using AutoMapper;
using AutoMapper.Configuration.Annotations;

namespace Services.Common
{
    /// <summary>
    /// General cancellation token for endpoint's input
    /// </summary>
    public class HasCancellationToken
    {
        // ReSharper disable once UnassignedGetOnlyAutoProperty
        [Ignore, IgnoreMap] public CancellationToken CancellationToken { get; }
    }
}