﻿using System;

namespace Services.Common
{
    /// <summary>
    /// When record that we want is not exist, or already deleted
    /// </summary>
    public class NotFoundException : Exception
    {
    }

    /// <summary>
    /// When sql unique index or constraint is violated
    /// </summary>
    public class UniqueIndexOrConstraintException : Exception
    {
        public string FieldName { get; }

        public UniqueIndexOrConstraintException(string fieldName, string message) : base(message)
        {
            FieldName = fieldName;
        }
    }
}