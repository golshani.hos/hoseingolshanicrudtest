﻿namespace Data.Helpers
{
    public class UniqueConstraints
    {
        public const string CustomersFirstNameLastNameDateOfBirth =
            "IX_UNQ_Customers_FirstName_LastName_DateOfBirth";
        
        public const string CustomersEmail =
            "IX_UNQ_Customers_Email";
    }
}