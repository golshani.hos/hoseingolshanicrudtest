﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Data.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Data.Helpers
{
    public static class Extensions
    {
        private const int SqlServerViolationOfUniqueIndex = 2601;
        private const int SqlServerViolationOfUniqueConstraint = 2627;

        private static readonly Regex UniqueConstraintRegex =
            new Regex(
                @"\ACannot insert duplicate key row in object \'(?<TableName>.+?)\' with unique index \'(?<IndexName>.+?)\'\. The duplicate key value is \((?<KeyValues>.+?)\)",
                RegexOptions.Compiled);


        public static IQueryable<T> WhereExists<T>(this IQueryable<T> queryable)
            where T : IAuditing
        {
            return queryable.Where(r => !r.IsDeleted);
        }

        public static IQueryable<T> WhereExists<T>(this IQueryable<T> queryable, Expression<Func<T, bool>> predicate)
            where T : IAuditing
        {
            return queryable.WhereExists().Where(predicate);
        }

        public static async Task SaveChangesAsyncEnsureAffectedRows(this DbContext dbContext,
            CancellationToken cancellationToken)
        {
            var affectedRows = await dbContext.SaveChangesAsync(cancellationToken);

            if (affectedRows == 0)
                throw new Exception();
        }

        public static string GetViolatedConstraintName(this Exception exception)
        {
            var dbUpdateEx = exception as DbUpdateException;
            if (!(dbUpdateEx?.InnerException is SqlException sqlEx))
                return null;

            if (sqlEx.Number != SqlServerViolationOfUniqueIndex &&
                sqlEx.Number != SqlServerViolationOfUniqueConstraint)
                return null;

            var match = UniqueConstraintRegex.Match(sqlEx.Message);

            if (!match.Success)
            {
                // todo: Log.Warning("maybe sql-server changes index error message")
                return null;
            }
            
            return match.Groups["IndexName"].Value;
        }
    }
}