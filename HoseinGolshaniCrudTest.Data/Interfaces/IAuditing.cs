﻿using System;

namespace Data.Interfaces
{
    public interface IAuditing
    {
        /// <summary>
        /// Time of record creation
        /// </summary>
        DateTime CreationTime { get; set; }

        /// <summary>
        /// Is record deleted?
        /// </summary>
        bool IsDeleted { get; set; }

        /// <summary>
        /// If record deleted, indicates when it was deleted 
        /// </summary>
        DateTime? DeletionTime { get; set; }

        /// <summary>
        /// If record has been modified at least once, indicates when it modified last time 
        /// </summary>
        DateTime? LastModificationTime { get; set; }
    }
}