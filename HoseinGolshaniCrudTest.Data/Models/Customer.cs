﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using Data.Helpers;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Data.Models
{
    public class Customer : IAuditing
    {
        public const int FirstAndLastNameMaxLength = 50;
        public const int EmailMaxLength = 100;
        public const int BankAccountNumberMaxLength = 30;

        public Customer()
        {
            CreationTime = DateTime.Now;
        }

        /// <summary>
        /// Customers' unique identifier number
        /// </summary>
        [Key, Required]
        public int Id { get; set; }

        /// <summary>
        /// Customer's legal first name
        /// </summary>
        [Required]
        [MaxLength(FirstAndLastNameMaxLength)]
        public string FirstName { get; set; }

        /// <summary>
        /// Customer's legal last name
        /// </summary>
        [Required]
        [MaxLength(FirstAndLastNameMaxLength)]
        public string LastName { get; set; }

        /// <summary>
        /// Customer's date of birth
        /// </summary>
        [Required]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Customer's phone number
        /// </summary>
        [Required]
        public ulong PhoneNumber { get; set; }

        /// <summary>
        /// Customer's personal email
        /// </summary>
        [MaxLength(EmailMaxLength)]
        public string? Email { get; set; }

        /// <summary>
        /// Customer's bank account number
        /// </summary>
        [MaxLength(BankAccountNumberMaxLength)]
        public string? BankAccountNumber { get; set; }

        /// <summary>
        /// Time of record creation
        /// </summary>
        [Required]
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// Is record deleted?
        /// </summary>
        [Required]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// If record deleted, indicates when it was deleted 
        /// </summary>
        public DateTime? DeletionTime { get; set; }

        /// <summary>
        /// If record has been modified at least once, indicates when it modified last time 
        /// </summary>
        public DateTime? LastModificationTime { get; set; }

        public class EntityConfiguration : IEntityTypeConfiguration<Customer>
        {
            public void Configure(EntityTypeBuilder<Customer> builder)
            {
                builder.HasIndex(r => r.Email)
                    .IsUnique()
                    .HasFilter("[Email] IS NOT NULL AND [IsDeleted] = 0")
                    .HasName(UniqueConstraints.CustomersEmail);
                
                builder.HasIndex(r => new {r.FirstName, r.LastName, r.DateOfBirth})
                    .IsUnique()
                    .HasFilter("[IsDeleted] = 0")
                    .HasName(UniqueConstraints.CustomersFirstNameLastNameDateOfBirth);
            }
        }
    }
}