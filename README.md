# Hosein Golshani’s Crud Test

It's my CRUD code challenge result.

I use 3 projects for these chllange, and apply onion architecture:

1. HoseinGolshaniCrudTest: as Host
2. HoseinGolshaniCrudTest.Services: for Services Layer 
3. HoseinGolshaniCrudTest.Data: for Data Layer

I don’t config some other things like Logger and APM.

To getting started:

1. Migrate (Default CS is: Data Source=.;Initial Catalog=HoseinGolshaniCrudTest;Integrated Security=True;)
2. Run 'HoseinGolshaniCrudTest'
